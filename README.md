# Apache NiFi Registry

This is an image to run [Apache NiFi Registry](https://nifi.apache.org/registry.html). It was based on the [Apache NiFi image](https://hub.docker.com/r/apache/nifi/)

## Running

Run the command:

```
docker run -p 18080:18080 domrock/nifi-registry
```

Then open you browser [localhost:18080/nifi-registry](localhost:18080/nifi-registry)
